const motionQuery = window.matchMedia('(prefers-reduced-motion: reduce)')
motionQuery.addEventListener('change', function () {
  if (motionQuery.matches) { pauseVideos() }
})

function pauseVideos () {
  document.querySelectorAll('video.loaded').forEach(video => {
    if (video.muted && video.loop) {
      video.pause()
      video.setAttribute('controls', true)
    }
  })
}

function updateSources (element) {
  if (element.dataset.src) {
    console.log('SRC: updating ' + element.dataset.src)
    element.src = element.dataset.src
    element.removeAttribute('data-src')
  }
  if (element.dataset.srcset) {
    console.log('SRCSET: updating ' + element.dataset.srcset)
    element.srcset = element.dataset.srcset
    element.removeAttribute('data-srcset')
  }
}

function lazyload (media) {
  if (media.tagName === 'VIDEO') {
    if (media.muted && media.loop) {
      media.load()
      media.setAttribute('controls', false)
      if (!motionQuery.matches) {
        media.play()
      } else {
        media.setAttribute('controls', true)
      }
      console.log('lazyloaded ' + media.id)
    }
  } else if (media.tagName === 'PICTURE') {
    const sources = Array.from(media.children)
    sources.forEach(updateSources)
  } else {
    updateSources(media)
  }
}

function callback (entries, observer) {
  entries.forEach(entry => {
    if (entry.isIntersecting) {
      const heroMedia = entry.target.querySelector('summary video, img')
      console.log('loading hero media')
      lazyload(heroMedia)
      heroMedia.classList.add('loaded')
      observer.unobserve(entry.target)
    }
  })
}

if ('IntersectionObserver' in window) {
  const detailsElements = document.querySelectorAll('details')
  const observer = new IntersectionObserver(callback)

  detailsElements.forEach((element) => {
    observer.observe(element)

    element.addEventListener('mouseenter', (event) => { lazyloadContent(event) }, { once: true, passive: true })
    element.querySelector('summary').addEventListener('focus', (event) => { lazyloadContent(event) }, { once: true, passive: true })
    element.addEventListener('toggle', (event) => { lazyloadContent(event) }, { once: true, passive: true })
  })
}
function lazyloadContent (event) {
  console.log(event)
  const target = (event.target.tagName === 'SUMMARY') ? event.target.closest('details') : event.target
  const content = target.querySelector('.content')
  if (content.classList.contains('loaded')) return

  const media = target.querySelectorAll('.content video, iframe, picture, :not(picture) > img')
  Array.from(media).forEach((mediaElement) => {
    console.log('loading media element')
    lazyload(mediaElement)
    mediaElement.classList.add('loaded')
  })
}
